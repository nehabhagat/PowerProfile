CPP=arm-none-linux-gnueabi-g++
CC=arm-none-linux-gnueabi-gcc

all: gprs.o ridlr_wifi_comm.o
	${CPP} -g -Wall -o gprs ridlr_wifi_comm.o gprs.o

ridlr_wifi_comm.o: ridlr_wifi_comm.cpp ridlrwificomm.h
	${CPP} -g -Wall -c ridlr_wifi_comm.cpp 

gprs.o: gprs.cpp
	${CPP} -g -Wall -c gprs.cpp

clean:
	rm -rf *.o gprs
