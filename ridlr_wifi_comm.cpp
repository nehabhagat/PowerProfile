#include "ridlrwificomm.h"

using namespace std;


RidlrWifiComm::RidlrWifiComm(string file ):serialPort(0),filepath(file){

    memset (recvBuf,0,sizeof(recvBuf));

    //init map
    commandMap[AT_QIFGCNT] = "AT+QIFGCNT";
    commandMap[AT_QICSGP] = "AT+QICSGP";
    commandMap[AT_QIREGAPP] = "AT+QIREGAPP\r\n";
    commandMap[AT_QIACT] = "AT+QIACT\r\n";

}

RidlrWifiComm::~RidlrWifiComm( ) {

    close(serialPort);
}

bool RidlrWifiComm::init() {
    /*------------------------------- Opening the Serial Port -------------------------------*/


        //fd = open(ACMFILE,O_RDWR | O_NOCTTY | O_NDELAY);	/* ttyUSB0 is the FT232 based USB2SERIAL Converter   */
                                    /* O_RDWR Read/Write access to serial port           */
                                    /* O_NOCTTY - No terminal will control the process   */
                                    /* O_NDELAY -Non Blocking Mode,Does not care about-  */
                                    /* -the status of DCD line,Open() returns immediatly */

        serialPort = open(filepath.c_str(), O_RDWR | O_NOCTTY|O_NDELAY );	/* ttyUSB0 is the FT232 based USB2SERIAL Converter   */

        if(serialPort == -1) {

           perror("Open error")	;				/* Error Checking */
           printf("\n  Error! in Opening %s\n",filepath.c_str());

           return false;
        }

         tcflush(serialPort, TCIOFLUSH);
        struct termios serialPortSettings;	/* Create the structure */
        memset(&serialPortSettings, 0, sizeof(serialPortSettings));
//        tcgetattr(serialPort, &serialPortSettings);	/* Get the current attributes of the Serial port */

        cfsetispeed(&serialPortSettings,B115200); /* Set Read  Speed as 115200                      */
        cfsetospeed(&serialPortSettings,B115200); /* Set Write Speed as 115200                      */

        serialPortSettings.c_cflag &= ~PARENB;   /* Disables the Parity Enable bit(PARENB),So No Parity   */
        serialPortSettings.c_cflag &= ~CSTOPB;   /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
        serialPortSettings.c_cflag &= ~CSIZE;	 /* Clears the mask for setting the data size             */
        serialPortSettings.c_cflag |=  CS8;      /* Set the data bits = 8                                 */

        serialPortSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
        serialPortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */


        //serialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);          /* Disable XON/XOFF flow control both i/p and o/p */
        serialPortSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);  /* Non Cannonical mode                            */
        //serialPortSettings.c_iflag &= ~( ECHO | ECHOE | ISIG);  /* Cannonical mode                            */


    serialPortSettings.c_oflag &= ~OPOST;/*No Output Processing*/
    /* Setting Time outs */
    serialPortSettings.c_cc[VMIN] = 0; /* Read at least 10 characters */
    serialPortSettings.c_cc[VTIME] = 100; /* Wait for 0.3   */



        if((tcsetattr(serialPort,TCSANOW,&serialPortSettings)) != 0) { /* Set the attributes to the termios structure*/

            printf("\n  ERROR ! in Setting attributes");
            return false;
        }

//    if((tcsetattr(serialPort, TCSAFLUSH, &serialPortSettings)) != 0 ) {

//        printf("\n  ERROR ! in Setting attributes2 ");
//            return false;

//    }

//    delay(30);



#if 1
    int flags = fcntl(serialPort, F_GETFL, 0);

    if (flags == -1) {

        printf("ERROR in getting flags\n");
        return false;
    }

        flags &= ~O_NDELAY;

    if ( fcntl(serialPort, F_SETFL, flags) < 0 ){

        printf("ERROR in setting flags\n");
        return false;
    }
#endif
        return true;
}

int RidlrWifiComm::ridlrSerialSend(const char* psOutput,int len) {


    short bytesWritten = 0;

    tcflush(serialPort, TCIOFLUSH);

    bytesWritten = write(serialPort,psOutput,len);

    if (bytesWritten < 0 ) {

            perror("Write failure\n");

    		return -1;
    }

    printf("TX: %s ByteCount: %d\n\n",psOutput,bytesWritten);

    return 0;
}

int RidlrWifiComm::ridlrSerialReceive () {

	short int bytesRead = 0;

    	char  retVal = 0 ;

    	memset(recvBuf, 0, sizeof(recvBuf) );

    	bytesRead = read( serialPort, recvBuf,sizeof(recvBuf));

    	if (bytesRead < 0 ) {

		printf("Failure in read\n");

		return -1;

    	}

        else if ( bytesRead  == 0 ) {

               printf(" Read timer expires\n");
               //Received EOF
               retVal = -1;
        }

        else {

		printf("Buffer %s\n",recvBuf);


    	}

    	return retVal;
}


int RidlrWifiComm::parseString( char * cmd_str, char * dest, unsigned char max_len,const char *delim ) {


        char * token = NULL;

        token = strtok(cmd_str,delim );

        if (token) {

        memcpy ( dest, token, max_len );

        }
        else {

                return -1;
        }

        return 0;

}


char * RidlrWifiComm::getRecvBuf(){

    return recvBuf;
}

void delay(int milliseconds)
{
    long pause;
    clock_t now,then;

    pause = milliseconds*(CLOCKS_PER_SEC/1000);
    now = then = clock();
    while( (now-then) < pause )
        now = clock();
}
