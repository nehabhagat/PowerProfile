#include<iostream>
#include<stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "ridlrwificomm.h"

#define PWR_MGMT_DEVICE "/dev/led_pwm"
#define ACMFILE "/dev/ttymxc4"

#define GPRS_POWER              _IO('p', 0xbb)
#define GPRS_POWER_OFF          _IO('p', 0xbf)

int activateGPRS();

int sendReceive ( string cmdStr );

RidlrWifiComm ridlrWifiComm(ACMFILE);

int gprsPowerIn( int gprsOn ) {

	int pwr_fd = -1;

        pwr_fd = open(PWR_MGMT_DEVICE, O_RDONLY);
	
	if ( pwr_fd < 0 ){
		
		printf("Open PWR_MGMT_DEVICE failure\n");

		return -1;

	}

		
	if ( gprsOn ) {

		printf("REQ:: GPRS Power ON\n");
	
        	if ( ioctl(pwr_fd,GPRS_POWER_OFF ,1) < 0 ) {

			printf("gprs_power_off failure\n");

			return -1;
		}

        	if ( ioctl(pwr_fd,GPRS_POWER ,1) < 0 ) {
			
			printf("GPRS_POWER_ON failure\n");

			return -1;

		}

		
		printf("SUCCESS :: GPRS Power ON\n");

		delay(10000);

		if ( activateGPRS() < 0 ){

			printf("Failure in activateGPRS\n");

			return -1;
		}

	}

	else {
		printf("REQ:: GPRS Power OFF\n");

		if ( ioctl(pwr_fd, GPRS_POWER_OFF ,1) < 0 ) {

			printf("gprs_power_off failure\n");

			return -1;
		}
		
		printf("SUCCESS :: GPRS Power OFF\n");
		
	}
	
        close(pwr_fd);

	return 0;

}

int  activateGPRS() {


	//Send AT+QIFGCNT=0

	printf("Sending AT+QIFGCNT .............\n");

	string cmdStr = "AT+QIFGCNT=0\r\n";

	if (sendReceive(cmdStr) < 0 ){

		printf("sendReceive AT+QIFGCNT failure \n");

		return -1;	
	}

	delay(10);

	//AT+QICSGP	
	printf("Sending AT+QICSGP .............\n");

	cmdStr = "AT+QICSGP=1,\"airtelgprs.com\"";

	if (sendReceive(cmdStr) < 0 ){

		printf("sendReceive AT+QIFGCNT failure \n");

		return -1;	
	}


	return 0;
		
}

int sendReceive ( string cmdStr ) {


	if ( ridlrWifiComm.ridlrSerialSend (cmdStr.c_str(),cmdStr.length()) != 0 ) {

        	printf("%s ridlrSerialSend Failure\n",__func__);

		return -1;
        }

	if ( ridlrWifiComm.ridlrSerialReceive()  < 0 ) {


		printf("%s ridlrSerialReceive Failure\n",__func__);

		return -1;


	}

	return 0;


}

int main(int argc, char * argv[]) {


	int gprsOn = 0, noOfIterations = 0 , interval = 0;

	if (argc != 4 ){

		printf("gprs : Usage <gprsONflag(0/1)> <NoOfIterations> <interval(sec)>\n");

		exit(1);
	}

	gprsOn = atoi(argv[1]);

	noOfIterations = atoi(argv[2]);
	
	interval = atoi(argv[3]);

	printf("Command IP : <gprsOn>:%d , <noOfIterations>:%d <interval(sec)>:%d\n",gprsOn,noOfIterations,interval);



	if ( ridlrWifiComm.init() == false ) {	


			printf("ridlrWifiComm failed\n");	
		
			exit(1);

		}

	for (int i = 0; i<noOfIterations; i++ ) {

	
		if (  gprsPowerIn( gprsOn ) != 0 ) {


			printf("GPRS power Input failed\n");	
		
			exit(1);

		}

		printf("Waiting for %d sec\n",interval);
		delay(interval * 1000);
		

		printf("########### End of iteration %d ################\n",i);

	}
		
	printf("########### End of test ################\n");

}
