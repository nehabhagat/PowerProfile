#ifndef RIDLRWIFICOMM_H
#define RIDLRWIFICOMM_H


#include "string"
#include <stdio.h>
#include <fcntl.h>   /* File Control Definitions           */
#include <termios.h> /* POSIX Terminal Control Definitions */
#include <unistd.h>  /* UNIX Standard Definitions 	   */
#include <errno.h>   /* ERROR Number Definitions           */
#include <string.h>   /* String functions                  */
#include <stdlib.h>   /* String functions                  */
#include <map>
#define MAX_SERIAL_DATA 50
#define MAX_RETRY_COUNT 20
#define MAX_TEMP_BUF 20
#define MAX_MAC_STRING 20
#define MAX_TICKETS 1000
#define MAX_SIZE_OF_TICKET 160
#define SIZE_RECV_BUF      MAX_SIZE_OF_TICKET*MAX_TICKETS
#define RIDLR_OK "OK"
void delay(int milliseconds);

typedef enum {

    AT_QIFGCNT = 0,
    AT_QICSGP,
    AT_QIREGAPP,
    AT_QIACT


}CommandCode;


using namespace std;

class RidlrWifiComm
{
    private:
        int serialPort;
        string filepath;
    char recvBuf[SIZE_RECV_BUF+1];

    map <CommandCode, string> commandMap;

    int ridlrSerialConnect ( long Baud_Rate,long Data_Bits, long Parity,long Stop_Bits);

        
    int parseString( char * cmd_str, char * dest, unsigned char max_len,const char *delim );


    public:
        RidlrWifiComm ( string file );
        ~RidlrWifiComm ();
        bool init();

    int ridlrSerialSend(const char* psOutput,int len);

    int ridlrSerialReceive ();


    void ridlrSendCMD (CommandCode cmd ,string cmdParams = "",void * response = NULL);

    char * getRecvBuf();

    string retCode;

};


#endif // RIDLRWIFICOMM_H
